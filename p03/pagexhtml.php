<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title> Práctica 3</title>
</head>
<body>
<p style="font-size:200%; text-align:center;"> Práctica 3 </p>
<hr/>

<?php
echo "<h3> 1. Variable válida o inválida </h3>";
echo "<h5> $ _ myvar Válido por iniciar con underscore </h5>";
echo "<h5> $ _ 7var Válido por iniciar con underscore </h5>";
echo "<h5> myvar Inválido por no iniciar con $ </h5>";
echo "<h5> $ myvar Válido por iniciar con $ </h5>";
echo "<h5> $ var7 Válido por iniciar con $ </h5>";
echo "<h5> $ _ element1 Válido por iniciar con underscore </h5>";
echo "<h5> $ house *5 Inválido por uso de * ASCII </h5><hr/>";

///////////////////////////////////////////////////////////////////////
echo "<h3> 2. Proporcionar valores </h3>";
echo "<h5> Valores iniciales </h5>";
$a = "ManejadorSQL";
$b = 'MySQL';
$c = &$a;
echo "$a, $b, $c";

echo "<h5> Valores con cambio, debido a las referencias en valores c y b hacia a que obtuvo un nuevo valor 'PHP server'</h5>";
$a = "PHP server";
$b = &$a;
echo "$a, $b, $c";
unset($a, $b, $c); 

echo "<hr/>";
///////////////////////////////////////////////////////////////////////

echo "<h3> 3. Mostrar contenido y verificar evolución </h3>";
echo "<h5> Asignación de valores y muestra </h5>";
$a = "PHP5";
echo "Variable a: ", var_dump($a), "<br>";
$z[] = &$a;
echo "Arreglo z: ", var_dump($z), "Arreglo de valor referenciado de 'a'<br>";
$b = "5a version de PHP";
echo "Variable b: ", var_dump($b), "<br>";
$c = (int)$b*10;
echo "Variable c: ", var_dump($c), "Operacion de b(5 unico valor int) * 10<br>";

echo "<h5> Asignación de nueva cuenta para a b z </h5>";
$a .= $b;
echo "Variable a: ", var_dump($a), "Donde agrega valor de b = 5a version de PHP<br>";
$b = (int)$b*$c;
echo "Variable b: ", var_dump($b), "Cambia debido a la operacion b(5 unico valor int)*c(50) <br>";
$z[0] = "MySQL";
echo "Arreglo z: ", var_dump($z), "El valor 'a' pasa a ser 'MySQL' por lo que el arreglo ahora contiene este valor modificado<br>";
unset($a, $b, $c, $z); 

echo "<hr/>";
///////////////////////////////////////////////////////////////////////

echo "<h3> 4. Mostrar y leer contenido con $ GLOBALS </h3>";
echo "<h5> Asignación de valores y muestra </h5>";
$a = "PHP5";
$z[] = &$a;
$b = "5a version de PHP";
$c = (int)$b*10;

echo "<h5> En código se crea funcion que usa 'global' para ubicar e imprimir resultados </h5>";
function imprime(){
    global $a, $z, $b, $c;
    echo "Variable a: ", var_dump($a), "<br>";
    echo "Variable b: ", var_dump($b), "<br>";
    echo "Variable c: ", var_dump($c), "<br>";
    echo "Arreglo z: ", var_dump($z), "<br>";
}
imprime();

echo "<h5> Asignación de nueva cuenta para a b z </h5>";
$a .= $b;
$b = (int)$b*$c;
$z[0] = "MySQL";
imprime();

unset($a, $b, $c, $z); 

echo "<hr/>";
/////////////////////////////////////////////////////////////////////////////

echo "<h3> 5. Asignando valores y muestra para el final </h3>";
echo "<h5> Asignación de valores </h5>";
$a = "7 personas";
echo "Variable a: ", $a, "<br>";
$b = (integer) $a;
echo "Variable b: ", $b ," (Toma unicamente el valor 7 integer de 'a')<br><br>";
echo "Nueva asignación valor para 'a = 9E3'<br>";
$a = "9E3";
$c = (double) $a;
echo "Variable c: ", $c ," (Toma unicamente el valor 9 de 'a' y lo trabaja como double)";

echo "<h5> VALORES FINALES </h5>";
echo "Variable a: ", $a, "<br>";
echo "Variable b: ", $b, "<br>";
echo "Variable c: ", $c, "<br>";

unset($a, $b, $c); 

echo "<hr/>";
/////////////////////////////////////////////////////////////////////////////

echo "<h3> 6. Dar y comprobar el valor booleano de las variables </h3>";
echo "<h5> Asignación de valores a b c d e f </h5>";
$a = "0";
$b = "TRUE";
$c = FALSE;
$d = ($a OR $b);
$e = ($a AND $c);
$f = ($a XOR $b);


echo "Valor booleano de variables (var_dump): <br>";

echo "a: ", var_dump(boolval($a)), "<br>";
echo "b: ", var_dump(boolval($b)), "<br>";
echo "c: ", var_dump(boolval($c)), "<br>";
echo "d: ", var_dump(boolval($d)), "<br>";
echo "e: ", var_dump(boolval($e)), "<br>";
echo "f: ", var_dump(boolval($f)), "<br><br>";


echo "Valor booleano de 'c' y 'e': <br>";

echo "Uso de boolval";
echo '<br /> c: '.(boolval($c) ? 'true' : 'false');
echo '<br /> e: '.(boolval($e) ? 'true' : 'false');

unset($a, $b, $c, $d, $e, $f);
echo "<hr/>";
/////////////////////////////////////////////////////////////////////////////

echo "<h3> 7. Usando la variable predefinida $ _ SERVER </h3>";

echo "a. Versión de Apache y PHP <br>";
echo $_SERVER['SERVER_SOFTWARE'];

echo "<br><br>b. Nombre del sistema operativo (servidor) <br>";
echo $_SERVER["HTTP_USER_AGENT"]; 
 
echo "<br><br>c. Idioma del navegador (cliente) <br>";
echo $_SERVER['HTTP_ACCEPT_LANGUAGE'];
?>


<p>
    <a href="http://validator.w3.org/check?uri=referer"><img
      src="http://www.w3.org/Icons/valid-xhtml11" alt="Valid XHTML 1.1" height="31" width="88" /></a>
  </p>
</body>
</html>