<!DOCTYPE html >
<html>

<head>
    <meta charset="utf-8" >
    <title>Registro de productos</title>
    <style type="text/css">
      ol, ul { 
      list-style-type: none;
      }
    </style>

     <!-- VALIDACIÓN//////////////////////////////////////////////// -->
     <script type="text/javascript">

    function validarCampos(){
              var nombre  = document.getElementById('form-name').value;
              var marca   = document.getElementById('form-marca').value;
              var modelo  = document.getElementById('form-model').value;
              var precio  = document.getElementById('form-price').value;
              var detalles = document.getElementById('form-details').value;
              var unidades = document.getElementById('form-units').value;
              var imagen  = document.getElementById('form-img').value;


              var alfanumerico = /^[A-Za-z0-9\-\ ]+$/g;
              var decimal =  /^\d*(\.\d{3})?\d{0,1}$/;
              var extension = /\.(jpg|png)$/i;

              var valido = true;
  
                /*NOMBRE//////////////////*/
                if(!nombre || nombre.length <= 3){
                    alert(' Nombre no dado o muy corto.');
                    valido = false;
                /*100 CARACTERES*/
                } else if(nombre.length > 100){
                    alert(' Nombre del producto muy LARGO');
                    valido = false;
                }

                /* MARCA////////////////////////*/
                if(marca == ''){
                    alert('Marca NO seleccionada.');
                    valido = false;
                }

                /*MODELO//////////////////*/
                if(!modelo || modelo.length <= 5){
                    alert('Modelo no dado o muy corto');
                    valido = false;
                /*25 CARACTERES*/
                } else if(modelo.length > 25){
                    alert('Nombre del modelo muy LARGO');
                    valido = false;
                /*ALFANUMERICO*/
                } else if(!alfanumerico.test(modelo)){
                    alert('Campo modelo con caracteres INVALIDOS');
                    valido = false;
                } 

                /*PRECIO////////////////////////*/
                if(!precio){
                    alert('Precio del producto no dado.');
                    valido = false;
                /*MAYOR A...*/   
                } else if(precio <= 99.99){
                    alert('El precio debe ser mayor a 99.99');
                    valido = false;
                }

                 /*DETALLES///////////////*/
                if(detalles.length > 250){
                    alert('Detalles del producto muy extenso MAX 250 CARACTERES');
                    valido = false;
                }

                 /*UNIDADES/////////////////////*/
                if(!unidades){
                    alert('campo UNIDADES no dadas.');
                    valido = false;
                }else if(unidades < 0){
                    alert('El número de unidades disponibles del producto debe ser igual o mayor a 0.');
                    valido = false;
                }

                 /*IMAGEN////////////////////////*/
                if(!imagen){
                    imagen = "img/default.png";
                } else if(!extension.test(imagen)) {
                    alert('El archivo puesto NO es una imagen');
                    valido = false;
                }

    return valido;
}
</script>

  </head>

  <body>
    <h1>Registro de productos</h1>

    <p>Ingresa los datos del nuevo producto.</p>

    <form id="formulario" action="#" method="post" onsubmit="return validarCampos()">

    <h2>Información de PRODUCTO NUEVO</h2>

      <fieldset>
        <legend>Información</legend>

        <ul>
          <li><label for="form-name">Nombre:</label>
          <input type="text" name="name" id="form-name" value="<?= !empty($_POST['nombre'])?$_POST['nombre']:$_GET['nombre'] ?>">
          </li>
          <br>
          <li><label for="form-marca">Marca:</label>
          <select name="Marca" id="form-marca" value="<?= !empty($_POST['marca'])?$_POST['marca']:$_GET['marca'] ?>">
                            <option selected value=''>Escoge una marca</option>
                                <option >Hasbro</option> 
                                <option >FisherPrice</option> 
                                <option >Mattel</option>
                                <option >Barbie</option> 
                                <option >Disney</option> 
                                <option >Lego</option> 
                                <option >Marvel</option> 
                                <option >Nerf</option>
                                <option >Play-Doh</option>
                                <option >Generico</option>
                        </select>
          </li>
                        <!--<input type="Marca" name="Marca" id="Marca"></li>-->
          <br>
          <li><label for="form-model">Modelo:</label>
          <input type="Modelo" name="Modelo" id="form-model" value="<?= !empty($_POST['modelo'])?$_POST['modelo']:$_GET['modelo'] ?>">
          </li>
          <br>
          <li><label for="form-price">Precio:</label>
          <input step="any" type="number" name="precio" id="form-price" value="<?= !empty($_POST['precio'])?$_POST['precio']:$_GET['precio'] ?>">
          </li>
          <br>
          <li><label for="form-details">Detalles:</label><br>
          <textarea name="story" rows="4" cols="60" id="form-details" placeholder="No más de 250 caracteres de longitud"><?= !empty($_POST['detalles'])?$_POST['detalles']:$_GET['detalles'] ?></textarea>
          </li>
          <br>
          <li><label for="form-units">Unidades:</label>
          <input type="number" id="form-units" name="cantidad" min="1" max="50" value="<?= !empty($_POST['unidades'])?$_POST['unidades']:$_GET['unidades'] ?>">
          </li>
          <br>
          <li><label for="form-img">Imagen dirección:</label>
          <input type="img" name="img" id="form-img" value="img/default.png" value="<?= !empty($_POST['imagen'])?$_POST['imagen']:$_GET['imagen'] ?>">
          </li>
        </ul>
      </fieldset>

      <p>
        <input type="submit" value="Insertar" id="enviar">
        <input type="reset">
      </p>

    </form>
  </body>

</html>    