// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
  };

// FUNCIÓN CALLBACK DE BOTÓN "Buscar"
function buscarID(e) {
    /**
     * Revisar la siguiente información para entender porqué usar event.preventDefault();
     * http://qbit.com.mx/blog/2013/01/07/la-diferencia-entre-return-false-preventdefault-y-stoppropagation-en-jquery/#:~:text=PreventDefault()%20se%20utiliza%20para,escuche%20a%20trav%C3%A9s%20del%20DOM
     * https://www.geeksforgeeks.org/when-to-use-preventdefault-vs-return-false-in-javascript/
     */
    e.preventDefault();

    // SE OBTIENE EL ID A BUSCAR
    var id = document.getElementById('search').value;

    // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
    var client = getXMLHttpRequest();
    client.open('POST', './backend/read.php', true);
    client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    client.onreadystatechange = function () {
        // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
        if (client.readyState == 4 && client.status == 200) {
            console.log('[CLIENTE]\n'+client.responseText);
            
            // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
            let productos = JSON.parse(client.responseText);    // similar a eval('('+client.responseText+')');
            
            var r = Object.keys(productos).length;
            console.log('Registros: '+ r);

            // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
            for(variable of productos) {
                // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                let descripcion = '';
                    descripcion += '<li>precio: '+variable.precio+'</li>';
                    descripcion += '<li>unidades: '+variable.unidades+'</li>';
                    descripcion += '<li>modelo: '+variable.modelo+'</li>';
                    descripcion += '<li>marca: '+variable.marca+'</li>';
                    descripcion += '<li>detalles: '+variable.detalles+'</li>';
                
                // SE CREA UNA PLANTILLA PARA CREAR LA(S) FILA(S) A INSERTAR EN EL DOCUMENTO HTML
                let template = '';
                    template += `
                        <tr>
                            <td>${variable.id}</td>
                            <td>${variable.nombre}</td>
                            <td><ul>${descripcion}</ul></td>
                        </tr>
                    `;

                // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                document.getElementById("productos").innerHTML += template;
            }
        }
    };
    client.send("id="+id);
}

// FUNCIÓN CALLBACK DE BOTÓN "Agregar Producto"
function agregarProducto(e) {
    e.preventDefault();

    // SE OBTIENE DESDE EL FORMULARIO EL JSON A ENVIAR
    var productoJsonString = document.getElementById('description').value;

    // SE CONVIERTE EL JSON DE STRING A OBJETO
    var finalJSON = JSON.parse(productoJsonString);

    // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
    finalJSON['nombre'] = document.getElementById('name').value;

    // SE OBTIENE EL STRING DEL JSON FINAL
    productoJsonString = JSON.stringify(finalJSON,null,2);

    //VALIDACIÓN////////////////////////////////////////////////
   function validarCampos(){

             var val = eval('(' +  productoJsonString + ')');

             var alfanumerico = /^[A-Za-z0-9\-\ ]+$/g;
             var extension = /\.(jpg|png)$/i;

             var valido = true;
 
               /*NOMBRE//////////////////*/
               if(!val.nombre || val.nombre.length <= 3){
                   alert(' Nombre no dado o muy corto.');
                   valido = false;
               /*100 CARACTERES*/
               } else if(val.nombre.length > 100){
                   alert(' Nombre del producto muy LARGO');
                   valido = false;
               }

               /* MARCA////////////////////////*/
               if(val.marca == ''){
                   alert('Marca NO seleccionada.');
                   valido = false;
               }

               /*MODELO//////////////////*/
               if(!val.modelo || val.modelo.length <= 5){
                   alert('Modelo no dado o muy corto');
                   valido = false;
               /*25 CARACTERES*/
               } else if(val.modelo.length > 25){
                   alert('Nombre del modelo muy LARGO');
                   valido = false;
               /*ALFANUMERICO*/
               } else if(!alfanumerico.test(val.modelo)){
                   alert('Campo modelo con caracteres INVALIDOS');
                   valido = false;
               } 

               /*PRECIO////////////////////////*/
               if(!val.precio){
                   alert('Precio del producto no dado.');
                   valido = false;
               /*MAYOR A...*/   
               } else if(val.precio <= 99.99){
                   alert('El precio debe ser mayor a 99.99');
                   valido = false;
               }

                /*DETALLES///////////////*/
               if(val.detalles.length > 250){
                   alert('Detalles del producto muy extenso MAX 250 CARACTERES');
                   valido = false;
               }

                /*UNIDADES/////////////////////*/
               if(!val.unidades){
                   alert('campo UNIDADES no dadas.');
                   valido = false;
               }else if(val.unidades < 0){
                   alert('El número de unidades disponibles del producto debe ser igual o mayor a 0.');
                   valido = false;
               }

                /*IMAGEN////////////////////////*/
               if(!val.imagen){
                   imagen = "img/default.png";
               } else if(!extension.test(val.imagen)) {
                   alert('El archivo puesto NO es una imagen');
                   valido = false;
               }

   return valido;
}

    if(validarCampos()){

         // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
        var client = getXMLHttpRequest();

        client.open('POST', './backend/create.php', true);
        client.setRequestHeader('Content-Type', "application/json;charset=UTF-8");
        client.onreadystatechange = function () {
        
        // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
        if (client.readyState == 4 && client.status == 200) {
            //console.log(client.responseText);

            var respuesta = client.responseText;

            //Se muestra en una ventana de alerta la respuesta
             alert(respuesta);
            }
        };
        client.send(productoJsonString);  //Se envía el producto a registrar
    }
}

// SE CREA EL OBJETO DE CONEXIÓN COMPATIBLE CON EL NAVEGADOR
function getXMLHttpRequest() {
    var objetoAjax;

    try{
        objetoAjax = new XMLHttpRequest();
    }catch(err1){
        /**
         * NOTA: Las siguientes formas de crear el objeto ya son obsoletas
         *       pero se comparten por motivos historico-académicos.
         */
        try{
            // IE7 y IE8
            objetoAjax = new ActiveXObject("Msxml2.XMLHTTP");
        }catch(err2){
            try{
                // IE5 y IE6
                objetoAjax = new ActiveXObject("Microsoft.XMLHTTP");
            }catch(err3){
                objetoAjax = false;
            }
        }
    }
    return objetoAjax;
}

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;
}