<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
    <meta charset="utf-8" >
    <title>Registro de productos</title>
    <style type="text/css">
      ol, ul { 
      list-style-type: none;
      }
    </style>
  </head>

  <body>
    <h1>Registro de productos</h1>

    <p>Ingresa los datos del nuevo producto.</p>

    <form id="formularioTenis" action="http://localhost/tw/practicas/p_06/set_producto_v2" method="post">

    <h2>Información de PRODUCTO NUEVO</h2>

      <fieldset>
        <legend>Información</legend>

        <ul>
          <li><label for="form-name">Nombre:</label> <input type="text" name="name" id="form-name"></li>
          <br>
          <li><label>Marca:</label> <input type="Marca" name="Marca" id="Marca"></li>
          <br>
          <li><label>Modelo:</label> <input type="Modelo" name="Modelo" id="Modelo"></li>
          <br>
          <li><label for="precio">Precio:</label> <input input size="5" id="precio" name="precio" type="text"></li>
          <br>
          <li><label for="form-story">Detalles:</label><br><textarea name="story" rows="4" cols="60" id="form-story" placeholder="No más de 300 caracteres de longitud"></textarea></li>
          <br>
          <li><label for="quantity">Unidades:</label> <input type="number" id="quantity" name="cantidad" min="1" max="50"></li>
          <br>
          <li><label>Imagen dirección:</label> <input type="img" name="img" id="img"></li>
        </ul>
      </fieldset>

      <p>
        <input type="submit" name="submit" value="Insertar">
        <input type="reset">
      </p>

    </form>
  </body>

</html>    