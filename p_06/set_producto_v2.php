<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>Registro Completado</title>
		<style type="text/css">
			body {margin: 20px; 
			background-color: #C4DF9B;
			font-family: Verdana, Helvetica, sans-serif;
			font-size: 90%;}
			h1 {color: #005825;
			border-bottom: 1px solid #005825;}
			h2 {font-size: 1.2em;
			color: #4A0048;}
		</style>
</head>
<body>
		<h1>Estado de la inserción</h1>
	
		<h2>Datos ingresados:</h2>
		<ul>
			<li><strong>Nombre:</strong> <em><?php echo $_POST['name']; ?></em></li>
			<li><strong>Marca:</strong> <em><?php echo $_POST['Marca']; ?></em></li>
			<li><strong>Modelo:</strong> <em><?php echo $_POST['Modelo']; ?></em></li>
            <li><strong>Precio:</strong> <em><?php echo $_POST['precio']; ?></em></li>
            <li><strong>Detalles:</strong> <em><?php echo $_POST['story']; ?></em></li>
            <li><strong>Unidades:</strong> <em><?php echo $_POST['cantidad']; ?></em></li>
            <li><strong>Imagen dirección:</strong> <em><?php echo $_POST['img']; ?></em></li>
		</ul>
        <br>
	</body>

<?php
$nombre = $_POST['name'];
$marca  = $_POST['Marca'];
$modelo = $_POST['Modelo'];
$precio = $_POST['precio'];
$detalles = $_POST['story'];
$unidades = $_POST['cantidad'];
$imagen   = $_POST['img'];
$eliminado = 0;

/** SE CREA EL OBJETO DE CONEXION */
@$link = new mysqli('localhost', 'root', 'Sailorm00n', 'marketzone');	

/** comprobar la conexión */
if ($link->connect_errno) 
{
    die('Falló la conexión: '.$link->connect_error.'<br/>');
    /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
}

/** Crear una tabla que no devuelve un conjunto de resultados */
$sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}', {$eliminado})";

if ($nombre!=NULL and strlen($nombre) >= 3 and $marca!=NULL and strlen($marca) >= 3 and $modelo!=NULL and strlen($modelo) >= 3 and $detalles!=NULL and strlen($detalles) >= 20 and $imagen!=NULL and strlen($imagen) >= 3 and $link->query($sql) ) 
{
        echo '<h2>Producto INSERTADO con ID: <h2>'.$link->insert_id;   
}
else
{
    if(empty($nombre)){
        echo "<p>Campo 'Nombre' vacio</p>";
    } else {
        if(strlen($nombre) < 3){
        echo "<p>Campo 'Nombre' muy corto</p>";
        }
    }

    if(empty($marca)){
        echo "<p>Campo 'Marca' vacio</p>";
    }else{
        if(strlen($marca) < 3){
        echo "<p>Campo 'Marca' muy corto</p>";
        }
    }

    if(empty($modelo)){
        echo "<p>Campo 'Modelo' vacio</p>";
    }else{
        if(strlen($modelo) < 3){
        echo "<p>Campo 'Modelo' muy corto</p>";
        }
    }

    if(empty($precio)){
        echo "<p>Campo 'Precio' vacio</p>";
    }
    
    if(empty($detalles)){
        echo "<p>Campo 'Detalles' vacio</p>";
    }else{
        if(strlen($detalles) < 20){
        echo "<p>Campo 'Detalles' muy corto 20 caracteres al menos</p>";
        }
    }

    if(empty($unidades)){
        echo "<p>Campo 'Unidades' vacio</p>";
    }

    if(empty($imagen)){
        echo "<p>Campo 'Imagen' vacio</p>";
    }else{
        if(strlen($imagen) < 3){
        echo "<p>Campo 'Imagen' muy corto para ser ubicacion de imagen</p>";
        }
    }

    echo '<br><br><h2>PRODUCTO NO INSERTADO</h2>';
}

$link->close();
?>
</html>